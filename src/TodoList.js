const TodoList = ({ list, remove }) => {
    return (
      <>
        {list?.length > 0 ? (
          <ul className="todo-list">
            {list.map((item, index) => (
              <div key={index} className="todo">
                <li key={item.id}> {item.title} </li>
                <button
                  className="delete-button"
                  onClick={() => {
                    remove(item.id);
                  }}
                >
                  Delete
                </button>
              </div>
            ))}
          </ul>
        ) : (
          <div className="empty">
            <p>No task found</p>
          </div>
        )}
      </>
    );
  };

export default TodoList;
