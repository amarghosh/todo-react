import { useState, useEffect } from "react";
import TodoInput from "./TodoInput";
import TodoList from "./TodoList";
import { get, put, del } from 'aws-amplify/api';
import "./App.css";

const App = () => {
  const [todos, setTodos] = useState([]);
  const [todo, setTodo] = useState("");
  async function getTodos() {
    try {
      const { body } = await get({
        apiName: 'amarghoshtodo',
        path: '/todo',
      }).response;
      const data = await body.json();
      console.log("Succeded: " + JSON.stringify(data));
      setTodos(data);
    } catch (error) {
      console.log('GET call failed: ', error);
    }
  };
  async function addTodo() {
    if (todo !== "") {
      console.log("ToDo: " + todo)
      try {
        const todoReq = { id: new Date().toString(), title: todo };
        console.log(todoReq)
        const restOperation = put({
          apiName: 'amarghoshtodo',
          path: '/todo',
          options: {
            body: todoReq
          }
        });
        const response = await restOperation.response;
        console.log('PUT call succeeded: ', response);
        getTodos();
      } catch (err) {
        console.log('PUT call failed: ', err);
      }
    }
  };
  async function deleteTodo (id) {
    try {
      const { statusCode } = await del({
        apiName: 'amarghoshtodo',
        path: '/todo/object/' + id,
      }).response;
      getTodos();
    } catch (error) {
      console.log('GET call failed: ', error);
    }
  };

  useEffect(() => {
    getTodos()
  }, [])

  return (
    <div className="App">
      <h1>React Todo App</h1>
      <TodoInput todo={todo} setTodo={setTodo} addTodo={addTodo} />
      <TodoList list={todos} remove={deleteTodo} />
      Version: 1.3
    </div>
  );
};
export default App;
